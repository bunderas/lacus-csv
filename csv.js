var imageList

fs = require('fs');
fs.readFile('aminek_az_AA_27es_oszlopba_kell_kerulnie.csv', 'utf8', listLoaded);

function listLoaded(e, data) {
  imageList = data.split('\r\n')

  fs2 = require('fs');
  fs2.readFile('talan_utolso.csv', 'utf8', dataLoaded);

}

function dataLoaded(e, data) {
  var startFrom = 1 // 1 ha van fejlec, 0 ha nincs
  var contents = data;
  var rows = contents.split('\r\n')
  var colSep = ";"
  // console.log(rows[startFrom])
  var lastId = rows[startFrom].split(colSep)[1].split('-')[0] // elso hasznalhato sorbol kiszedni az id-t
  var lastIdx = startFrom
  var data = []
  var tgt = [];
  if (startFrom == 1) {
    tgt.push(rows[0])
  }
  for (var i = startFrom +1 ; i < rows.length; i++) {
    parts = rows[i].split(colSep)
    if (parts.length == 1) continue // ures sor kihagyasa
    idParts = parts[1].split('-')
    if (lastId !== idParts[0]) {
      imgs = imageList.filter(imageFilter, lastId) // kepek kikeresese
      if (i - lastIdx == 1) { // ha nincs ismetlodes
        lastRow = rows[lastIdx].split(colSep);
        lastRow[26] = imgs.join(',') // kepek beszurasa
        tgt.push('simple'+ lastRow.join(colSep)+ colSep) // simple sor
      } else { // ismetlodes volt
        newRows = rows[i-1].slice().split(colSep); // utolso variation kiszedese, ez belemegy a beszurando sorba
        newRows[1] = lastId // id beirasa 
        newRows[26] = imgs.join(',') // kepek beszurasa
        sizes = [] 
        for (var j = lastIdx; j<i; j++) { // meretek kigyujtese
          sizes.push(rows[j].split(colSep)[38])           
        }          
        newRows[38] = sizes.join(',') // meretek a megfelelo cellaba
        tgt.push('variable' + newRows.join(colSep)+colSep); // uj sor beszurasa
        for (var j = lastIdx; j<i; j++) {
          tgt.push('variation' + rows[j]+colSep+lastId) // variation-ok atirasa az uj tombbe, hozzaadva a variable id
        }          
      }
      lastIdx = i; // last idx update
      lastId = idParts[0] // lastId update
    }
  }
  // file kiirasa
  var fs3 = require('fs');
  fs3.writeFile("result.csv", tgt.join('\r\n'), 'utf8', function(err) {
      if(err) {
          return console.log(err);
      }

      console.log("The file was saved!");
  }); 
};

function imageFilter(item) {
  regex= new RegExp("\/"+this+"(\\s|_)")
  return regex.test(item)
}
